import pip
import numpy as np
import os
try:
    from PIL import Image
except ImportError as e:
    pip.main(['install', 'pillow'])

def show(data):
    img = Image.frombytes('1', data.shape[::-1], np.packbits(data, 1))
    img.save("/home/rstudio/rstudio-templates/Python_Mandelbrot.png")
    #img.show()

n = 100
maxiter = 100

M = np.zeros([n, n], np.uint8)
xvalues = np.linspace(-2, 2, n)
yvalues = np.linspace(-2, 2, n)

for u, x in enumerate(xvalues):
    for v, y in enumerate(yvalues):
        z = 0
        c = complex(x, y)
        for i in range(maxiter):
            z = z*z + c
            if abs(z) > 2.0:
                M[v, u] = 1
                break

#print("Working directory: ", os.getcwd())
show(M)
print("Output saved as file Python_Mandelbrot.jpg")


